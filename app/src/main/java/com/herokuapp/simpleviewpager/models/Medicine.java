package com.herokuapp.simpleviewpager.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by alfred on 7/9/14.
 */
@DatabaseTable(tableName = "medicine")
public class Medicine {
    /**
     * ormLite default constructor
     */
    //fields not part of table
    public static  final String DRUG_NAME_FIELD = "name";
    public static final String DRUG_DESCRIPTION_FIELD = "description";
    public static final String DRUG_ID_FIELD = "id";

   //fields part of table
    @DatabaseField
    public String name;

    @DatabaseField(id=true, generatedId = false)
    public int id;

    @DatabaseField
    public String description;

    public Medicine(){

    }

    public Medicine(int id) {
        this.id = id;
    }
    public Medicine(int id, String name){
        this.name = name;
        this.id = id;
    }
    public Medicine(int id, String name, String description){
        this.name = name;
        this.id = id;
        this.description = description;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
