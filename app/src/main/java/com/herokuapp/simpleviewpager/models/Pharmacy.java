package com.herokuapp.simpleviewpager.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by alfred on 7/12/14.
 */
@DatabaseTable(tableName = "pharmacy")
public class Pharmacy {

    //default constructor
    public  Pharmacy(){
    }

    /**
     *
     * @param id
     * @param name
     * @param username
     * @param email
     * @param address
     * @param telephone
     * @param lat
     * @param lng
     */
    public Pharmacy(int id,  String name, String username,
                    String email, String address, String telephone,
                    String lat, String lng){
        this.id = id;
        this.name = name;
        this.username = username;
        this.email = email;
        this.address = address;
        this.telephone = telephone;
        this.lat = lat;
        this.lng = lng;
    }
    @DatabaseField(id = true,generatedId = false)
    public int id;

    @DatabaseField
    public String email;

    @DatabaseField
    public double distance;

    @DatabaseField
    public String name;

    @DatabaseField
    public String lat;

    @DatabaseField
    public String lng;

    @DatabaseField
    public String telephone;

    @DatabaseField
    public String address;

    @DatabaseField
    public String username;

    @Override
    public String toString() {
        return String.valueOf(id);
    }
}
