package com.herokuapp.simpleviewpager;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.Html;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.herokuapp.simpleviewpager.helper.DaoFactory;
import com.herokuapp.simpleviewpager.models.Medicine;
import com.herokuapp.simpleviewpager.models.Pharmacy;
import com.herokuapp.simpleviewpager.utils.ConnectionDetector;
import com.herokuapp.simpleviewpager.utils.GPSLocationData;
import com.herokuapp.simpleviewpager.utils.SphericalUtil;
import com.herokuapp.simpleviewpager.views.UIHelper;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.jar.Attributes;


public class MyActivity extends FragmentActivity implements AdapterView.OnItemClickListener
, View.OnClickListener{
    private ViewPager _mViewPager;
    private ViewPagerAdapter _adapter;
    AutoCompleteTextView autoComplete;
    List<Medicine> drugs;
    ArrayList<String> drugNames;
    TextView textView1, textView2;
    LatLng start, end;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    GPSLocationData locationData = new GPSLocationData(this);
        start = new LatLng(locationData.latitude, locationData.longitude);
        drugNames = new ArrayList<String>();

        DaoFactory factory = new DaoFactory(this);
        drugs = factory.getAllDrugs();
        for(Medicine m : drugs){
            drugNames.add(m.name);
        }

        setUpView();
        setTab();
        initialize();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("name",
                UIHelper.getTextFromTextView(MyActivity.this, R.id.tv1));
        outState.putString("description",
                UIHelper.getTextFromTextView(MyActivity.this, R.id.tv2));
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        UIHelper.displayText(
                MyActivity.this, R.id.tv1,
                savedInstanceState.getString("name"));
        UIHelper.displayText(
                MyActivity.this, R.id.tv2,
                savedInstanceState.getString("description")
        );
        super.onRestoreInstanceState(savedInstanceState);
    }



    private void setUpView(){
        _mViewPager = (ViewPager) findViewById(R.id.viewPager);
        _adapter = new ViewPagerAdapter(getApplicationContext(),getSupportFragmentManager());
        _mViewPager.setAdapter(_adapter);
        _mViewPager.setCurrentItem(0);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.textView1:
                _mViewPager.setCurrentItem(0);
                break;
            case R.id.textView2:
                _mViewPager.setCurrentItem(1);
                break;
        }

    }
    private void setTab(){
        _mViewPager.setOnPageChangeListener(new OnPageChangeListener(){


            @Override
            public void onPageScrollStateChanged(int position) {}
            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {}
            @Override
            public void onPageSelected(int position) {
                switch(position){
                    case 0:
                        findViewById(R.id.first_tab).setVisibility(View.INVISIBLE);
                        findViewById(R.id.second_tab).setVisibility(View.VISIBLE);
                        break;

                    case 1:
                        findViewById(R.id.first_tab).setVisibility(View.VISIBLE);
                        findViewById(R.id.second_tab).setVisibility(View.INVISIBLE);
                        break;
                }
            }

        });

    }




    public void initialize(){
        textView1 = (TextView)findViewById(R.id.textView1);
        textView2 = (TextView)findViewById(R.id.textView2);
        textView2.setOnClickListener(this);
        textView1.setOnClickListener(this);
        autoComplete = (AutoCompleteTextView) findViewById(R.id.autoComplete);
        autoComplete.setOnItemClickListener(this);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(MyActivity.this,
                android.R.layout.simple_list_item_1,drugNames);
        autoComplete.setAdapter(adapter);



    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String name = (String)parent.getAdapter().getItem(position);
        UIHelper.displayText(this, R.id.tv1, name);
        Medicine medicines = new DaoFactory(this).getMedicineByName(name);
        UIHelper.displayText(this,
                R.id.tv2, medicines.description);
        UIHelper.hideKeyboard(this, autoComplete);
        if (new ConnectionDetector(MyActivity.this).isConnectingToInternet()){
            makeAjaxCall2("http://ipharm.herokuapp.com/api/find/?id="+medicines.id);
        }else{

            Toast.makeText(this, "there is no internet connection", Toast.LENGTH_LONG).show();

        }



    }
    public void makeAjaxCall2(String url){
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setTitle("loading pharmacies...");
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setCancelable(false);
        dialog.show();

        Ion.with(this)
                .load(url)
                .progressDialog(dialog)
                .asJsonArray()
                .setCallback(new FutureCallback<JsonArray>() {
                    @Override
                    public void onCompleted(Exception e, JsonArray jsonElements) {
                        dialog.dismiss();
                        DaoFactory factory = new DaoFactory(MyActivity.this);
                        factory.dropAllPharmacies();
                        for(int i = 0 ; i<jsonElements.size(); i++)
                        {

                            try {
                                JsonObject jsonObject = jsonElements.get(i).getAsJsonObject();
                                String email = jsonObject.get("email").getAsString();
                                String name = jsonObject.get("name").getAsString();
                                String telephone = jsonObject.get("telephone").getAsString();
                                String address = jsonObject.get("address").getAsString();
                                String lat = jsonObject.get("lat").getAsString();
                                String lng = jsonObject.get("lng").getAsString();
                                Log.e("lat", lat);
                                Log.e("lng", lng);
                                end = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));
                                double distance = SphericalUtil.computeDistanceBetween(start, end);
                                int id = Integer.parseInt(jsonObject.get("id").getAsString());
                                String username = jsonObject.get("username").getAsString();
                                Pharmacy pharmacy = new Pharmacy(id,name,username,email,address, telephone,
                                        lat, lng);
                                pharmacy.distance = distance;
                                DaoFactory myFactory = new DaoFactory(MyActivity.this);
                                myFactory.savePharmacy(pharmacy);


                            }catch (NullPointerException nullPointer )
                            {nullPointer.printStackTrace();}
                            catch (NumberFormatException num){num.printStackTrace();}


                        }


                        DaoFactory factory1 = new DaoFactory(MyActivity.this);
                        List<Pharmacy> pharmacies = factory1.getAllPharmacies(true);
                        TextView[] myTextViews = new TextView[pharmacies.size()];

                        LinearLayout l = (LinearLayout)findViewById(R.id.linear);


                        if(pharmacies == null || (pharmacies.toString().equals("[]")))
                        {
                            l.removeAllViews();
                            TextView tv = new TextView(MyActivity.this);
                            tv.setText("No pharmacy has this drug");
                            tv.setTextAppearance(MyActivity.this,R.style.large);
                            l.addView(tv);

                        }
                        else
                        {
                            l.removeAllViews();
                            for(int i = 0 ; i<pharmacies.size();i++)
                            {
                                StringBuilder details = new StringBuilder();
                                details.append("<h2>Name: "+pharmacies.get(i).name+"</h2>");
                                details.append("<h5>Address: "+pharmacies.get(i).address+"</h5>");
                                details.append("<h5>Telephone: "+pharmacies.get(i).telephone+"</h5>");
                                details.append("<h5>Email: "+pharmacies.get(i).email+"</h5><br>");
                                details.append("<h5>distance: "+pharmacies.get(i).distance+"</h5><br>");
                                myTextViews[i] = new TextView(MyActivity.this);
                                myTextViews[i].setText(Html.fromHtml(details.toString()));
                                myTextViews[i].setTextAppearance(MyActivity.this, R.style.text_view);
                                myTextViews[i].setTag(pharmacies.get(i).email);
                                myTextViews[i].setOnClickListener(new ViewOnclickListenerItem());
                                l.addView(myTextViews[i]);

                            }

                        }












                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.help:

                startActivity(new Intent(this, WalkThroughActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }





}