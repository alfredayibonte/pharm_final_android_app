package com.herokuapp.simpleviewpager;

/**
 * Created by alfred on 8/14/14.
 */
import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.herokuapp.simpleviewpager.helper.DaoFactory;
import com.herokuapp.simpleviewpager.models.Pharmacy;
import com.herokuapp.simpleviewpager.route.Route;
import com.herokuapp.simpleviewpager.route.Routing;
import com.herokuapp.simpleviewpager.route.RoutingListener;
import com.herokuapp.simpleviewpager.utils.GPSLocationData;

public class LocationActivity extends FragmentActivity implements RoutingListener {

    private GoogleMap map;
    Pharmacy pharmacy;
    LatLng start, end;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String email = getIntent().getExtras().getString("email");

        DaoFactory factory = new DaoFactory(this);
        pharmacy = factory.getPharmacyByEmail(email);
        if(servicesOk()){

            setContentView(R.layout.activity_location);
            if(initMap()){
                double lat, lng;
                try{

                    lat = Double.parseDouble(pharmacy.lat);
                    lng = Double.parseDouble(pharmacy.lng);

                }catch (NullPointerException nullPointer)
                {
                    Toast.makeText(this, "unknown location",
                            Toast.LENGTH_LONG).show();
                    lat = 5.55571;
                    lng = -0.19630;
                }
                catch (NumberFormatException numberFormat){
                    Toast.makeText(this, "unknown location",
                            Toast.LENGTH_LONG).show();
                    lat = 5.55571;
                    lng = -0.19630;
                }

                GPSLocationData locationData = new GPSLocationData(this);
                 end = new LatLng(lat, lng);
                 start = new LatLng(locationData.latitude, locationData.longitude);
                map.addMarker(new MarkerOptions().position(end).title(pharmacy.name));
                map.setMyLocationEnabled(true);
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(end, 15));
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(end, 15));
                map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
//
//
//               Polyline line = map.addPolyline(new PolylineOptions()
//                        .add(new LatLng(locationData.latitude, locationData.longitude), new LatLng(lat, lng))
//                        .width(5)
//                        .color(Color.BLUE));

                Routing routing = new Routing(Routing.TravelMode.DRIVING);
                routing.registerListener(this);
                routing.execute(start, end);






                Toast.makeText(this,pharmacy.name, Toast.LENGTH_LONG).show();
            }else{ Toast.makeText(this, "map not available", Toast.LENGTH_LONG).show();}
        }else{
            setContentView(R.layout.main);
        }




    }

    public  boolean servicesOk(){
        int isAvailable = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (isAvailable == ConnectionResult.SUCCESS)
        {
            return true;
        }
        else if (GooglePlayServicesUtil.isUserRecoverableError(isAvailable))
        {
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(isAvailable, this, 9001);
            dialog.show();
        }else {
            Toast.makeText(this, "can not connect to google play service", Toast.LENGTH_LONG).show();
        }
        return false;
    }

    public boolean initMap(){
        if(map == null){
            SupportMapFragment mapFragment =
                    (SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map);
            map = mapFragment.getMap();
        }
        return (map != null);
    }


    @Override
    public void onRoutingFailure() {

    }

    @Override
    public void onRoutingStart() {

    }

    @Override
    public void onRoutingSuccess(PolylineOptions mPolyOptions, Route route) {
        PolylineOptions polyOptions = new PolylineOptions();
        polyOptions.color(Color.BLUE);
        polyOptions.width(10);
        polyOptions.addAll(mPolyOptions.getPoints());
        map.addPolyline(polyOptions);

        // Start marker
        MarkerOptions options = new MarkerOptions();
        options.position(start);
        options.icon(BitmapDescriptorFactory.fromResource(R.drawable.start_blue));
        map.addMarker(options);

        // End marker
        options = new MarkerOptions();
        options.position(end);
        options.icon(BitmapDescriptorFactory.fromResource(R.drawable.end_green));
        map.addMarker(options);

    }
}