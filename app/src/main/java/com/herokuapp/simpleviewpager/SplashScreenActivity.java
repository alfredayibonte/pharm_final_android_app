package com.herokuapp.simpleviewpager;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.herokuapp.simpleviewpager.helper.DaoFactory;
import com.herokuapp.simpleviewpager.models.Medicine;
import com.herokuapp.simpleviewpager.utils.ConnectionDetector;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.List;


public class SplashScreenActivity extends Activity {
    private static final String TAG= SplashScreenActivity.class.getSimpleName();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);

            if (new ConnectionDetector(
                    SplashScreenActivity.this).isConnectingToInternet()
                    )
            {
                makeAjaxCall(getResources().getString(R.string.drug_url));
            } else
            {
                Toast.makeText(SplashScreenActivity.this,
                        "The internet is not connect", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(SplashScreenActivity.this, MyActivity.class);
                startActivity(intent);
                finish();


            }




    }

    /**
     * makes a call to the drugs api
     * @param url
     * @return response
     */
    public void makeAjaxCall(String url)
    {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("loading drugs..");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.show();

        Ion.with(this)
                .load(url)
                .progressDialog(progressDialog)
                .asJsonArray()
                .setCallback(new FutureCallback<JsonArray>() {
                    @Override
                    public void onCompleted(Exception e, JsonArray result) {
                        progressDialog.dismiss();
                        if (e != null ){
                            Toast.makeText(SplashScreenActivity.this,
                                    "error in connection", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(SplashScreenActivity.this, MyActivity.class);
                            startActivity(intent);
                            finish();

                        }
                        saveDrugToDataBase(result);
                    }
                });

    }

    /**
     * converts response into Json and saves into Drugs database
     * @param result
     */
    private  void saveDrugToDataBase(JsonArray result)  {

        if(result != null )
        {
            JsonArray jsonArray = result.getAsJsonArray();

            for(int i = 0 ; i<jsonArray.size(); i++)
            {

                try {
                    JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
                    int id = Integer.parseInt(jsonObject.get("id").getAsString());
                    String name = jsonObject.get("name").getAsString();
                    String description = jsonObject.get("description").getAsString();
                    Medicine drug = new Medicine(id, name, description);
                    DaoFactory myFactory = new DaoFactory(this);
                    myFactory.saveDrug(drug);
                }catch (NullPointerException nullPointer )
                {nullPointer.printStackTrace();}
                catch (NumberFormatException num){num.printStackTrace();}


            }


        }
        Intent intent = new Intent(SplashScreenActivity.this, MyActivity.class);
        startActivity(intent);
        finish();




    }





}
