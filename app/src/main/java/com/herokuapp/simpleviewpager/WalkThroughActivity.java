package com.herokuapp.simpleviewpager;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by alfred on 8/16/14.
 */
public class WalkThroughActivity extends FragmentActivity implements View.OnClickListener{
    ImageView imageView;
    ViewPager viewPager;
    StepsViewPagerAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.walk_through);
        initialize();
        setUpView();
        setTab();
    }
    public void initialize(){
       imageView =  (ImageView)findViewById(R.id.back);
        imageView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent i = new Intent(this, MyActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);

    }
    private void setUpView(){
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        adapter = new StepsViewPagerAdapter(getApplicationContext(),getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(0);
    }

    private void setTab(){
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener(){


            @Override
            public void onPageScrollStateChanged(int position) {}
            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {}
            @Override
            public void onPageSelected(int position) {
                switch(position){
                    case 0:
                        findViewById(R.id.first_tab).setVisibility(View.INVISIBLE);
                        findViewById(R.id.second_tab).setVisibility(View.VISIBLE);
                        break;

                    case 1:
                        findViewById(R.id.first_tab).setVisibility(View.VISIBLE);
                        findViewById(R.id.second_tab).setVisibility(View.INVISIBLE);
                        break;
                }
            }

        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
}
