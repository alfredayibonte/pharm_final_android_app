
package com.herokuapp.simpleviewpager;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class PharmaciesFragment extends Fragment {


	public static Fragment newInstance(Context context) {
		PharmaciesFragment f = new PharmaciesFragment();
		
		return f;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		ViewGroup root = (ViewGroup) inflater.inflate(R.layout.pharmacies, null);

      return root;
	}


	
}
