package com.herokuapp.simpleviewpager.route;

//. by Haseem Saheed
public interface Parser {
    public Route parse();
}