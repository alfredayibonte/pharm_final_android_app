package com.herokuapp.simpleviewpager;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

public class ViewPagerAdapter extends FragmentStatePagerAdapter
{
	private Context _context;
	
	public ViewPagerAdapter(Context context, FragmentManager fm) {
		super(fm);	
		_context=context;
		
		}
	@Override
	public Fragment getItem(int position) {
		Fragment f = new Fragment();
		switch(position){
		case 0:
			f= DrugFragment.newInstance(_context);
			break;
		case 1:
			f= PharmaciesFragment.newInstance(_context);
			break;
		}
		return f;
	}
	@Override
	public int getCount() {
		return 2;
	}

}
