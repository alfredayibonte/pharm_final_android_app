package com.herokuapp.simpleviewpager.views;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.TextView;

/**
 * Created by alfred on 7/9/14.
 */
public class UIHelper {


    /**
     * Function to display simple Alert Dialog
     * @param context - application context
     * @param title - alert dialog title
     * @param message - alert message
     * @param status - success/failure (used to set icon)
     * */
    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);

        // Setting Dialog Title
        alertBuilder.setTitle(title);

        // Setting Dialog Message
        alertBuilder.setMessage(message);

        // Setting alert dialog icon
        alertBuilder.setIcon(
                (status) ?
                        android.R.drawable.stat_notify_chat :
                        android.R.drawable.stat_notify_error
        );

        // Setting OK Button
        alertBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertBuilder.show();
    }
    public static void displayText(Activity activity, int id, String text) {
        TextView tv = (TextView) activity.findViewById(id);
        tv.setText(text);
    }

    public static String getTextFromTextView(Activity activity, int id) {
        TextView et = (TextView) activity.findViewById(id);
        return et.getText().toString().trim();
    }
    public static AutoCompleteTextView getAC(Activity activity, int id) {
        AutoCompleteTextView ac = (AutoCompleteTextView) activity.findViewById(id);
        return ac;
    }

    public static boolean getCBChecked(Activity activity, int id) {
        CheckBox cb = (CheckBox) activity.findViewById(id);
        return cb.isChecked();
    }

    public static void setCBChecked(Activity activity, int id, boolean value) {
        CheckBox cb = (CheckBox) activity.findViewById(id);
        cb.setChecked(value);
    }
    public static  void hideKeyboard(Activity activity, View v){
        InputMethodManager imm = (InputMethodManager)activity.getSystemService(activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }


}
