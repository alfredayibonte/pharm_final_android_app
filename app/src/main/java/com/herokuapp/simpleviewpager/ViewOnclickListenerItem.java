package com.herokuapp.simpleviewpager;

import android.content.Intent;
import android.view.View;


/**
 * Created by alfred on 8/14/14.
 */
public class ViewOnclickListenerItem implements View.OnClickListener {
    @Override
    public void onClick(View v) {


        Intent intent = new Intent(v.getContext(), LocationActivity.class);
        intent.putExtra("email", v.getTag().toString());
        v.getContext().startActivity(intent);


    }
}
