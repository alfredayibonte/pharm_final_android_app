package com.herokuapp.simpleviewpager;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by alfred on 8/16/14.
 */
public class StepsViewPagerAdapter extends FragmentPagerAdapter {
    private Context _context;

    public StepsViewPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        _context=context;

    }
    @Override
    public Fragment getItem(int position) {
        Fragment f = new Fragment();
        switch(position){
            case 0:
                f= FirstStepFragment.newInstance(_context);
                break;
            case 1:
                f= SecondStepFragment.newInstance(_context);
                break;
        }
        return f;
    }
    @Override
    public int getCount() {
        return 2;
    }

}
