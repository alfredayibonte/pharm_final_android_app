package com.herokuapp.simpleviewpager.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


import com.herokuapp.simpleviewpager.models.Medicine;
import com.herokuapp.simpleviewpager.models.Pharmacy;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Created by alfred on 7/9/14.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "medicine.db";
    private static final int DATABASE_VERSION = 1;
    private Dao<Medicine, Integer> medicineDao = null;
    private Dao<Pharmacy, Integer> pharmacyDao = null;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            Log.i(DatabaseHelper.class.getName(), "onCreate");
            TableUtils.createTable(connectionSource, Medicine.class);
            TableUtils.createTable(connectionSource, Pharmacy.class);

        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Can't create database", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            Log.i(DatabaseHelper.class.getName(), "onUpgrade");
            TableUtils.dropTable(connectionSource, Medicine.class, true);
            TableUtils.dropTable(connectionSource, Pharmacy.class, true);
            onCreate(db, connectionSource);
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Can't drop databases", e);
            throw new RuntimeException(e);
        }
    }

    public void clearPharmacyTable() throws SQLException
    {
        TableUtils.clearTable(connectionSource, Pharmacy.class);

    }

public Dao<Medicine, Integer> getMedicineDao()
        throws SQLException {
    if (medicineDao == null){
        medicineDao = getDao(Medicine.class);

    }
    return medicineDao;
}

public Dao<Pharmacy, Integer> getPharmacyDao()
            throws SQLException {
        if (pharmacyDao == null){
            pharmacyDao = getDao(Pharmacy.class);

        }
        return pharmacyDao;
    }

    @Override
    public void close() {
        super.close();
    }

}
