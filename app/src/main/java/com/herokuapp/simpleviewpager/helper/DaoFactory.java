package com.herokuapp.simpleviewpager.helper;


import android.content.Context;
import android.util.Log;


import com.herokuapp.simpleviewpager.models.Medicine;
import com.herokuapp.simpleviewpager.models.Pharmacy;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by alfred on 7/9/14.
 */
public class DaoFactory  {

    private DatabaseHelper databaseHelper = null;

    private Dao<Medicine, Integer> medicineDAO = null;

    private Dao<Pharmacy, Integer> pharmacyDAO = null;


    public DaoFactory(Context context) {
        databaseHelper = new DatabaseHelper(context);
    }

    public void saveDrug(Medicine m){
        try {
            medicineDAO = getMedicineDao();
            medicineDAO.createIfNotExists(m);

        } catch (SQLException e){
            e.printStackTrace();
        }finally {
            databaseHelper.close();
        }

    }
    public void dropDatabase(){
        try {
            medicineDAO = getMedicineDao();
            medicineDAO.executeRaw("DROP IF EXISTS medicine");

        } catch (SQLException e){
            e.printStackTrace();

        }finally {
            databaseHelper.close();
        }

    }
    public Medicine getMedicineByName(String name){
        List<Medicine> medicines = null;
        try {
            medicineDAO = getMedicineDao();
             medicines = medicineDAO.query(
                    medicineDAO.queryBuilder()
                    .where()
                    .eq(Medicine.DRUG_NAME_FIELD, name)
                    .prepare());

        } catch (SQLException e){
            e.printStackTrace();

        }finally {
            databaseHelper.close();
        }
        return medicines.get(0);
    }
    public Pharmacy getPharmacyByEmail(String email){
        List<Pharmacy> pharmacies = null;
        try {
            pharmacyDAO = getPharmacyDao();
            pharmacies = pharmacyDAO.query(
                    pharmacyDAO.queryBuilder()
                            .where()
                            .eq("email", email)
                            .prepare());

        } catch (SQLException e){
            e.printStackTrace();

        }finally {
            databaseHelper.close();
        }
        return pharmacies.get(0);
    }


    public Dao<Medicine, Integer> getMedicineDao() throws SQLException {
        if (medicineDAO == null) {
            medicineDAO = databaseHelper.getDao(Medicine.class);
        }
        return medicineDAO;
    }

    public Dao<Pharmacy, Integer> getPharmacyDao() throws SQLException {
        if (pharmacyDAO == null) {
            pharmacyDAO = databaseHelper.getDao(Pharmacy.class);
        }
        return pharmacyDAO;
    }

    public List<Medicine> getAllDrugs(){
        List<Medicine> drugs = null;
        try {
            medicineDAO = getMedicineDao();
            drugs = medicineDAO.queryForAll();

        }catch (SQLException e){

        }finally {
            databaseHelper.close();
        }
        return drugs;
    }

    public List<Medicine> getAllDrugNames(){
        List<Medicine> drugs = null;
        try {

            medicineDAO = getMedicineDao();
            QueryBuilder<Medicine, Integer> builder = medicineDAO.queryBuilder();
            drugs = builder.selectColumns("name").query();

        }catch (SQLException e){

        }finally {
            databaseHelper.close();
        }
        for(int i =0 ; i< drugs.size(); i++){
            Log.e("Drugs", drugs.get(i).toString());
        }
        return drugs;
    }

    public List<Pharmacy> getAllPharmacies(){
        List<Pharmacy> pharmacies = null;
        try {
            pharmacyDAO = getPharmacyDao();
            pharmacies = pharmacyDAO.queryForAll();

        }catch (SQLException e){

        }finally {
            databaseHelper.close();
        }
        return pharmacies;
    }

    /**
     * gets all the pharmacies and order by distance
     * @param orderByDistance
     * @return
     */
    public List<Pharmacy> getAllPharmacies(boolean orderByDistance){
        List<Pharmacy> pharmacies = null;
        try {
            pharmacyDAO = getPharmacyDao();
            QueryBuilder<Pharmacy, Integer> builder = pharmacyDAO.queryBuilder();
            pharmacies = builder.orderBy("distance", orderByDistance).query();


        }catch (SQLException e){

        }finally {
            databaseHelper.close();
        }
        return pharmacies;
    }




    public void savePharmacy(Pharmacy p){
        try {
            pharmacyDAO = getPharmacyDao();
            pharmacyDAO.createIfNotExists(p);

        } catch (SQLException e){
            e.printStackTrace();
        }finally {
            databaseHelper.close();
        }

    }

    public void dropAllPharmacies(){
        try{
            databaseHelper.clearPharmacyTable();

        }
        catch (SQLException sqlException){
            sqlException.printStackTrace();
        }finally {
            databaseHelper.close();
        }

    }


    public void onTerminate() {
        if (databaseHelper != null) {
            databaseHelper.close();
            databaseHelper = null;
        }
    }
}
