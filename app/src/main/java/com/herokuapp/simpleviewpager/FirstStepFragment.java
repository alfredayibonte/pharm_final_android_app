package com.herokuapp.simpleviewpager;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by alfred on 8/16/14.
 */
public class FirstStepFragment extends Fragment {


    public static Fragment newInstance(Context context) {
        FirstStepFragment f = new FirstStepFragment();

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.first_step, null);

        return root;
    }

}